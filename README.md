# Instructions

## Pre-requisites

1. Create a new WordPress site, log in as admin.
2. Instal ACF from WP Plugin Repo.
3. Run "composer update" to download dependencies.

## Reproducing the Scenario

1. Activate this plugin.
2. Create a New Movie post.
3. Select something from the post drop-down, only one post should be available (https://1drv.ms/u/s!AuYDDdfI2CCi3pIu16RXqfxCMjn5qA)
4. Now go back to code and edit this file: inc/acf.php
5. Uncomment lines 19-21, save and reload the movie post you were editing.
6. Try and select something from the dropdown, nothing shows up, no error in console (https://1drv.ms/u/s!AuYDDdfI2CCi3pIvWKM9-vpxQtQLvw)