<?php
/**
 * Plugin Name: acf-possible-post-object-bug
 * Description: Plugin that shows an odd behaviour with ACF fields and the $_POST object
 *
 * @since  1.0
 *
 * @package scorpiotek-acf
 * Version: 1.0
 * Text Domain: scorpiotek
 **/

namespace ScorpioTek\Plugins;

// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

require_once 'vendor/autoload.php';

/**
 * Creates the custom post types required for the plugin.
 */
require_once 'inc/content-types.php';
/**
 * Renders all the ACF fields used bu the plugin.
 */
require_once 'inc/acf.php';




