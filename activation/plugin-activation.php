<?php
/** Contains the functionality to create the LGA database and fill it with the
 * corresponding data.
 */

global $lga_db_version;
$lga_db_version = '1.0';

/**
 * Code to include if you would like to create a table when the plugin is loaded.
 * If this is a mu_plugin, you must include:
 * add_action( 'mu_plugin_loaded', 'scorpiotek_table_creation', 10, 1 );
 * Otherwise:
 * register_activation_hook( __FILE__, 'scorpiotek_table_creation' );
 * register_deactivation_hook( __FILE__, 'scorpiotek_table_deletion' );
 *
 * @return void
 */

function scorpiotek_table_creation() {
	global $wpdb;
	global $lga_db_version;

	$table_name = $wpdb->prefix . 'lga';

	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE $table_name (
		entry_id INTEGER NOT NULL AUTO_INCREMENT,
        lga_state VARCHAR(255) NOT NULL,
        lga_region VARCHAR(255) NOT NULL,
		post_code INTEGER NOT NULL,
		PRIMARY KEY  ( entry_id )
		) $charset_collate;";

	require_once ABSPATH . 'wp-admin/includes/upgrade.php';
	dbDelta( $sql );
	add_option( 'lga_db_version', $lga_db_version );
}


function scorpiotek_table_deletion() {
	global $wpdb;
	$table_name = $wpdb->prefix . 'lga';
	$sql = "DROP TABLE IF EXISTS $table_name";
	require_once ABSPATH . 'wp-admin/includes/upgrade.php';
	$wpdb->query($sql);
	delete_option("lga_db_version");
}
