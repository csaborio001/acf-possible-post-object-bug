<?php
/**
 * Contains the definition of elements that will be loaded to offer plugin functionality.
 * These elements are scripts and stylesheets.
 *
 * @package compeer
 *
 */

/**
 * Loads the styles in the WPAdmin interface that modify the look and feel of the plugin's interface elements.
 *
 * @return void
 */
function load_custom_wp_plugin_admin_styles() {
	wp_register_style( 'admin_styles', plugin_dir_url( __DIR__ ) . 'assets/css/compeer-admin.css', false, '1.0.0.' );
	wp_enqueue_style( 'admin_styles' );
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_plugin_admin_styles' );

/**
 * Loads the scripts that are used by the WPAdmin interface.
 *
 * @return void
 */
function load_custom_wp_plugin_scripts() {
	$current_user = wp_get_current_user();
	if ( ! $current_user->exists() ) {
		return;
	}
	/** Assume the non-admin controls will be removed by default. */
	$remove_admin_controls = true;
	/** These are the allowed administrators */
	$admin_roles = array( 'administrator', 'compeer_admin' );
	/** We try to see if any of the roles of the currently logged user match the admin roles. */
	if ( 0 !== count( array_intersect( $admin_roles, $current_user->roles ) ) ) {
		/** If there is a match, then we remove the restriction. */
		$remove_admin_controls = false;
	}

	/** The script that disables the 'Change State' button needs to know
	 * the current state of the participant. The code belows sends this
	 * variable to the script file (state).
	 *
	 * The script also needs to know whether to hide the controls for non-admin users.
	 */

	if ( function_exists( 'get_current_screen' ) ) {
		$screen    = get_current_screen();
		$post_type = $screen->post_type;
		/** We need to do checks on every edit page that is not a post or a page. */
		if ( 'post' === $screen->base && ( 'post' !== get_post_type() || 'page' !== get_post_type() ) ) {
			/** These are the mods that change (usually disable) various elements in the
			 * WPAdmin interface based on various conditions.
			 */
			wp_register_script( 'participant_gui_mods', plugin_dir_url( __DIR__ ) . 'assets/js/participant-gui-mods.js', array( 'jquery' ), '1.0', true );
			wp_enqueue_script( 'participant_gui_mods' );
			/** Pass the variables to the JavaScript.*/
			wp_localize_script(
				'participant_gui_mods',
				'form_data',
				array(
					'state'                 => get_field( '_state' )['value'],
					'remove_admin_controls' => $remove_admin_controls,
				)
			);
		}
	}
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_plugin_scripts' );

