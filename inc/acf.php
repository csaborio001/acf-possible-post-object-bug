<?php
/**
 * Contains the definition of all the ACF fields used by the Social Justice Network.
 *
 * @since 1.0
 *
 * @package scorpiotek-social-media-blocks
 */

use \StoutLogic\AcfBuilder\FieldsBuilder;

if ( class_exists( FieldsBuilder::class ) ) {
	if ( class_exists( FieldsBuilder::class ) ) {
		/** By using $_GET we can find out whether post is being edited
		 * If it is not edited we immediately return.
		 */

		/** Uncomment 3 lines below to repro scenario. */
		// $current_post_type = $_GET['post_type'];
		// add_action( 'after_setup_theme', 'create_' . $current_post_type . '_fields' );
		// return;

		add_action( 'after_setup_theme', 'create_movie_fields' );

	}
}

/**
 * Register the fields that are used by the movies type.
 */
function create_movie_fields() {
	$demo_field_builder = new FieldsBuilder( 'demo_builder' );
	$demo_field_builder
		->addPostObject(
			'related_post',
			array(
				'label'        => 'The related post',
				'instructions' => 'Create some posts, then select a few from this list',
				'required'     => 1,
				'post_type'    => 'post',
				'multiple'     => 1,
			)
		)
		->setLocation( 'post_type', '==', 'movie' );
	add_action(
		'acf/init',
		function() use ( $demo_field_builder ) {
			acf_add_local_field_group( $demo_field_builder->build() );
		}
	);
}

